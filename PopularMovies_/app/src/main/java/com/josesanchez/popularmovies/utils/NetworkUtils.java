package com.josesanchez.popularmovies.utils;

import android.content.Context;
import android.net.Uri;

import com.josesanchez.popularmovies.R;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Scanner;

/**
 * Created by Skylaxx Studio on 02/07/2017.
 */

public class NetworkUtils {

    private static final String MOVIE_POSTER_BASE_URL="http://image.tmdb.org/t/p/w780";
    private static final String MOVIES_POPULAR_BASE_URL="https://api.themoviedb.org/3/movie/popular?api_key=65ab2cff50bc1c7642ee02c9780550d5";
    private static final String MOVIES_HIGHET_BASE_URL="https://api.themoviedb.org/3/movie/top_rated";
    private static final String PARAM_YOUR_API_KEY="api_key";

    public static URL buildUrl(String orderFilter) {
        URL url=null;
        Uri uri=  Uri.parse(MOVIE_POSTER_BASE_URL+orderFilter)
                .buildUpon()
                .build();
        try {
            url= new URL(uri.toString());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        return url;
    }

    public static URL buildMoviesUrl(int orderFilter, Context context) {

        String baseUrl="";
        switch (orderFilter)
        {
            case 0:
                baseUrl=MOVIES_POPULAR_BASE_URL;
                break;
            case 1:
                baseUrl=MOVIES_HIGHET_BASE_URL;
                break;
        }
        URL url=null;
        Uri uri=  Uri.parse(baseUrl)
                .buildUpon()
                .appendQueryParameter(PARAM_YOUR_API_KEY,context.getString(R.string.your_api_key))
                .build();
        try {
            url= new URL(uri.toString());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        return url;
    }

    public static String getResponseFromHttpUrl(URL url) throws IOException {
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        try {
            InputStream in = urlConnection.getInputStream();

            Scanner scanner = new Scanner(in);
            scanner.useDelimiter("\\A");

            boolean hasInput = scanner.hasNext();
            if (hasInput) {
                return scanner.next();
            } else {
                return null;
            }
        } finally {
            urlConnection.disconnect();
        }
    }
}
