package com.josesanchez.popularmovies.data;

import android.content.Context;
import android.media.Image;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.josesanchez.popularmovies.MoviesActivity;
import com.josesanchez.popularmovies.R;
import com.squareup.picasso.Picasso;
import java.net.ConnectException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Skylaxx Studio on 02/07/2017.
 */

public class MoviesAdapter extends BaseAdapter{
    private final Context mContext;
    private List<String> urlsMoviesPosters= new ArrayList<String>();

    public MoviesAdapter (Context context)
    {
        mContext=context;
        urlsMoviesPosters=null;//TODO setear la s urls correctas
        urlsMoviesPosters= MoviesActivity.urls;
    }

    @Override
    public int getCount() {
        return urlsMoviesPosters.size();
    }

    @Override
    public String getItem(int i) {
        return urlsMoviesPosters.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        ImageView poster= (ImageView)view;
        if(poster==null)
        {
            poster= new ImageView( mContext);
        }

        poster.setScaleType(ImageView.ScaleType.CENTER);

        String url=getItem(position);

        Picasso.with(mContext)
                .load(url)
                .placeholder(R.drawable.placeholder)
                .error(R.drawable.error)
                .tag(mContext)
                .into(poster);
        Log.i(this.getClass().getName(),"URL:"+url);

        return poster;
    }



}
