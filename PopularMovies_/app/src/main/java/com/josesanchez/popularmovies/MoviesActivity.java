package com.josesanchez.popularmovies;

        import android.content.Context;
        import android.content.Intent;
        import android.net.ConnectivityManager;
        import android.net.NetworkInfo;
        import android.os.AsyncTask;
        import android.support.annotation.BoolRes;
        import android.support.v7.app.AppCompatActivity;
        import android.os.Bundle;
        import android.support.v7.widget.RecyclerView;
        import android.util.Log;
        import android.view.View;
        import android.widget.AdapterView;
        import android.widget.ArrayAdapter;
        import android.widget.GridView;
        import android.widget.ProgressBar;
        import android.widget.Spinner;
        import android.widget.TextView;
        import android.widget.Toast;

        import com.josesanchez.popularmovies.data.Movie;
        import com.josesanchez.popularmovies.data.MoviesAdapter;
        import com.josesanchez.popularmovies.utils.NetworkUtils;

        import org.json.JSONArray;
        import org.json.JSONException;
        import org.json.JSONObject;

        import java.io.IOException;
        import java.net.URL;
        import java.util.ArrayList;
        import java.util.List;

public class MoviesActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener,AdapterView.OnItemClickListener{

    private final String POSTER_BASE_URL="http://image.tmdb.org/t/p/w780";

    public static List<String> urls;
    public List<Movie> moviesData;

    TextView mErrorDisplay;
    GridView mGridView;
    Spinner mSpinner;
    ProgressBar mProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movies);

        urls= new ArrayList<>();

        moviesData= new ArrayList<>();
        mErrorDisplay= (TextView) findViewById(R.id.tv_internet_error);
        mProgressBar= (ProgressBar) findViewById(R.id.progressBar);

        ArrayAdapter<CharSequence> mAdapter= ArrayAdapter.createFromResource(this,R.array.orders_array,R.layout.spinner_item);
        mAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSpinner= (Spinner) findViewById(R.id.sp_order_action);
        mSpinner.setAdapter(mAdapter);
        mSpinner.setOnItemSelectedListener(this);

        mGridView= (GridView) findViewById(R.id.rc_movies_grid_display);

        mGridView.setOnItemClickListener(this);



    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long l) {
        Log.i(MoviesActivity.class.getSimpleName(),"Spinner select item::"+pos);
        doMoviesPetition(pos);
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }


    public void doMoviesPetition(int pos)
    {
        URL url = NetworkUtils.buildMoviesUrl(pos,this);
        new MoviesListPetition().execute(url);
    }


    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
        Log.i(this.getLocalClassName(),"CLick en item");
        Intent detailsIntent= new Intent(this,MovieDetailActivity.class);
        detailsIntent.putExtra("titulo",moviesData.get(position).getTitle());
        detailsIntent.putExtra("rating",moviesData.get(position).getVoteAverage());
        detailsIntent.putExtra("synipsis",moviesData.get(position).getSynopsis());
        detailsIntent.putExtra("release",moviesData.get(position).getReleaseDate());
        detailsIntent.putExtra("poster",POSTER_BASE_URL+moviesData.get(position).getPosterUrl());

        startActivity(detailsIntent);
    }


    class MoviesListPetition extends AsyncTask<URL,Void,Boolean>
    {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading();
            urls.clear();
        }

        @Override
        protected Boolean doInBackground(URL... urls) {

            boolean isIntenet=false;

            URL url=urls[0];
            String reposnse=null;
            Log.i(MoviesActivity.class.getSimpleName(),"URL TO PETITION:"+url);

            if(isOnline())
            {
                try {
                    reposnse=NetworkUtils.getResponseFromHttpUrl(url);
                    Log.i(MoviesActivity.class.getSimpleName(),"RESPONSE:"+reposnse);
                    moviesData=getMoviesObjetcs(reposnse);
                    isIntenet=true;
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            else
            {
                Log.i(this.getClass().getSimpleName(),"No internet connection!");
                moviesData.clear();
                isIntenet=false;
            }


            return isIntenet;
        }

        @Override
        protected void onPostExecute(Boolean isInternet) {

            super.onPostExecute(isInternet);
            if(isInternet)// there is Internet!
            {

                mGridView.setAdapter(new MoviesAdapter(getApplicationContext()));
                showResults();
            }
            else
            {
//                Toast.makeText(getApplicationContext(),"No internet connection!",Toast.LENGTH_SHORT).show();
                showErrorInternet();
            }

        }
    }

    public List<Movie> getMoviesObjetcs(String apiResponse) throws JSONException {
        String  moviesResults="results";
        String  movieTitle="title";
        String  moviePoster="poster_path";
        String  releaseDate="release_date";
        String  voteAverage="vote_average";
        String  synopsis="overview";

        List<Movie> moviesReposnse= new ArrayList<Movie>();
        if(apiResponse!=null && !apiResponse.isEmpty())
        {
            JSONObject jsonResponse=new JSONObject(apiResponse);
            JSONArray movies= jsonResponse.getJSONArray(moviesResults);
            for (int i=0;i<movies.length();i++)
            {
                Movie mov= new Movie();

                mov.setTitle(movies.getJSONObject(i).getString(movieTitle));
                mov.setPosterUrl(movies.getJSONObject(i).getString(moviePoster));
                mov.setReleaseDate(movies.getJSONObject(i).getString(releaseDate));
                mov.setVoteAverage(movies.getJSONObject(i).getDouble(voteAverage));
                mov.setSynopsis(movies.getJSONObject(i).getString(synopsis));
                moviesReposnse.add(mov);
                urls.add(POSTER_BASE_URL+mov.getPosterUrl());

            }



        }

        for(Movie m:moviesReposnse)
        {
            Log.i("MOVIE:",m.toString());
        }
        return moviesReposnse;

    }

    public void showResults()
    {
        mErrorDisplay.setVisibility(View.INVISIBLE);
        mGridView.setVisibility(View.VISIBLE);
        mProgressBar.setVisibility(View.INVISIBLE);
    }

    public void showLoading()
    {
        mErrorDisplay.setVisibility(View.INVISIBLE);
        mProgressBar.setVisibility(View.VISIBLE);
        mGridView.setVisibility(View.INVISIBLE);
    }

    public void showErrorInternet()
    {
        mErrorDisplay.setVisibility(View.VISIBLE);
        mProgressBar.setVisibility(View.INVISIBLE);
        mGridView.setVisibility(View.INVISIBLE);
    }

    public void refresh(View view)
    {
        int posi=mSpinner.getSelectedItemPosition();
        doMoviesPetition(posi);
    }
    // this method I got it from the url: https://stackoverflow.com/questions/1560788/how-to-check-internet-access-on-android-inetaddress-never-times-out
    public boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }
}
