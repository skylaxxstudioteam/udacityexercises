package com.josesanchez.popularmovies;

import android.content.Intent;
import android.media.Image;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

public class MovieDetailActivity extends AppCompatActivity {

    private TextView title;
    private TextView rating;
    private TextView synopsis;
    private TextView release;
    private String posterUrl;
    private ImageView poster;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_detail);

        title= (TextView) findViewById(R.id.tv_title_movie);
        rating= (TextView) findViewById(R.id.tv_average_rating);
        synopsis= (TextView) findViewById(R.id.tv_synopsis);
        release= (TextView) findViewById(R.id.tv_release_date);
        poster= (ImageView) findViewById(R.id.iv_movie_poster_detail);

        Intent mIntent = getIntent();
        title.setText(mIntent.getStringExtra("titulo"));
        rating.setText(String.valueOf(mIntent.getDoubleExtra("rating",0)));
        synopsis.setText(mIntent.getStringExtra("synipsis"));
        release.setText(mIntent.getStringExtra("release"));
        posterUrl=mIntent.getStringExtra("poster");

        Log.i(this.getLocalClassName(),"cargando....."+posterUrl);
        Picasso.with(this)
                .load(posterUrl)
                .fit()
                .tag(this)
                .into(poster);


    }
}
